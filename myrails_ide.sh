#!/bin/bash
# myrails_ide.sh
#
# Script for setup tmux enviroment for rails development
# Obs.: Change the env vars
#
# Version 1.0: Initial version
# Paulo Jonathas, Oct 2017

DEV_PATH="~/myrecipes"
SESSION="myrecipes"
tmux -2 new-session -d -s $SESSION

# Development area
tmux rename-window "Development"
tmux send-keys "cd $DEV_PATH; vim" C-m

tmux split-window -p 20
tmux send-keys "cd $DEV_PATH" C-m

# Rails server
tmux new-window -n "Rails Server"
tmux send-keys "cd $DEV_PATH; rails server" C-m

# Comming back to Development area
tmux select-window -t $SESSION:0
tmux select-pane -t 0


# Reattach
tmux -2 attach-session -t $SESSION
